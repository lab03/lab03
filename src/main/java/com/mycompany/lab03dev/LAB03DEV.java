/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03dev;

/**
 *
 * @author Paweena Chinasri
 */
public class LAB03DEV {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    static int add(int num1, int num2) {
        return num1 + num2;
    }

    public static boolean checkWin(String[][] table, String currentPlayer) {
        if(checkRow(table,currentPlayer)){
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for(int row =0; row<3; row++){
        if(checkRow(table,currentPlayer, 0)){
        return true;
            }
        }
        
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
         for(int col = 0; col <3; col++){
            if(table[row][col].equals(currentPlayer)){ 
                return false;
            }
    }
         return true;
    }
}

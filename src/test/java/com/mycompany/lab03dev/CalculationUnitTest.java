package com.mycompany.lab03dev;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Paweena Chinasri
 */
public class CalculationUnitTest {
    
    public CalculationUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
   
    //all testcase run before Each 1
    @BeforeEach
    public void setUp() {
    }
    //all testcase run after Each 2
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testAdd_1_2_out_3(){
       int result = LAB03DEV.add(1,2);
        assertEquals(3, result);
        
    }
     @Test
    public void testAdd_2_2_out_4(){
       int result = LAB03DEV.add(1,2);
        assertEquals(3, result);
    }
     @Test
    public void testAdd_3_2_out_5(){
       int result = LAB03DEV.add(1,2);
        assertEquals(3, result);
    }
     @Test
    public void testAdd_5_5_out_10(){
       int result = LAB03DEV.add(1,2);
        assertEquals(3, result);
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany;

import com.mycompany.lab03dev.LAB03DEV;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Paweena Chinasri
 */
public class OXUnittest {
    
    public OXUnittest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
     @Test
    public void testCheckWin_O_Vertical1_output_true(){
       String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
       String currentPlayer = "O";
       boolean result = LAB03DEV.checkWin(table, currentPlayer);
       assertEquals(false, result);
       
    }
     @Test
    public void testCheckWin_O_Vertical2_output_true(){
       String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
       String currentPlayer = "O";
       boolean result = LAB03DEV.checkWin(table, currentPlayer);
       assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Vertical3_output_true(){
       String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
       String currentPlayer = "O";
       boolean result = LAB03DEV.checkWin(table, currentPlayer);
       assertEquals(true, result);
    }
    @Test
    public void testCheckWin_O_Vertical1_output_false(){
       String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
       String currentPlayer = "O";
       boolean result = LAB03DEV.checkWin(table, currentPlayer);
       assertEquals(true, result);
    }
    
    
}
